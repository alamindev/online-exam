<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::namespace('API')->middleware('auth:airlock')->group(function () {
    Route::get('questions', 'QuestionController@questions');
    Route::post('question/{question}', 'QuestionController@answer');
    Route::get('classes', 'ClassController@index');
    Route::get('sections/{classes}', 'ClassController@sections');
    Route::get('sets', 'SetController@index');
    Route::get('sets/{set}', 'SetController@details');
    Route::post('sets', 'SetController@store');
});
