## pre-requirement

PHP >= 7.2.5, MYSQL




## Clone project


## install dependency 

composer install

npm install && npm run prod 

## .env setup

SESSION_DRIVER=cookie

database setup

AIRLOCK_STATEFUL_DOMAINS='your_domain' #if php artisan serve then skip


## run

php artisan migrate --seed

php artisan serve

## access
Registration new user and check
or login by exiting db email and pass:password


:)


