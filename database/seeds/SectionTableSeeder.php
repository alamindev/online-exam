<?php

use Illuminate\Database\Seeder;

class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $six = \App\QClass::whereTitle('Six')->first();
        $seven = \App\QClass::whereTitle('Seven')->first();
        $seven =
            \App\Section::insert([
                [
                    'title' => "6a",
                    'class_id' => $six->id
                ],
                [
                    'title' => "6b",
                    'class_id' => $six->id
                ],
                [
                    'title' => "6c",
                    'class_id' => $six->id
                ],
                [
                    'title' => "6d",
                    'class_id' => $six->id
                ],
                [
                    'title' => "7a",
                    'class_id' => $seven->id
                ],
                [
                    'title' => "7b",
                    'class_id' => $seven->id
                ],
                [
                    'title' => "7c",
                    'class_id' => $seven->id
                ],
                [
                    'title' => "7d",
                    'class_id' => $seven->id
                ],
                [
                    'title' => "7e",
                    'class_id' => $seven->id
                ],

            ]);

    }
}
