<?php

use Illuminate\Database\Seeder;

class SetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Set::class, 5)->create()->each(function ($set) {
            $set->sections()->attach(\App\Section::limit(rand(3, 4))->pluck('id')->toArray());
            $set->questions()->saveMany(factory(\App\Question::class, 20)
                ->make())->each(function ($question) {
                return $question->options()->saveMany(factory(\App\Option::class, 4)->make());
            });
        });
    }
}
