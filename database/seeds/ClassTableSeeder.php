<?php

use Illuminate\Database\Seeder;

class ClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\QClass::insert([
            [
                'title' => "Six",
            ],
            [
                'title' => "Seven",
            ]
        ]);

    }
}
