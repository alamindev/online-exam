<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Set extends Model
{
    protected $guarded = [];

    public function sections()
    {
        return $this->belongsToMany(Section::class, 'set_section', 'set_id', 'section_id');
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }
}
