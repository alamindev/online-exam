<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QClass extends Model
{
    protected $guarded = [];
    protected $table='classes';

    public function sections()
    {
        return $this->hasMany(Section::class);
    }
}
