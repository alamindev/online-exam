<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $guarded = [];

    public function qclass()
    {
        return $this->belongsTo(QClass::class);
    }

    public function sets()
    {
        return $this->belongsToMany(Set::class, 'set_section', 'section_id','set_id');
    }
}
