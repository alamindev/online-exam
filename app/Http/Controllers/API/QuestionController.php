<?php


namespace App\Http\Controllers\API;


use App\Answer;
use App\Http\Requests\AnswerRequest;
use App\Option;
use App\Question;

class QuestionController
{
    public function questions($setId = 1)
    {
        return Question::with('options', 'answers')->inRandomOrder('id')->whereSetId($setId)->get();
    }

    /**
     * @param Question $question
     * @param Request $request
     * @return json;
     */

    public function answer(Question $question, AnswerRequest $request)
    {
        return $question->answers()->createMany($this->processRequest($request));
    }

    private function processRequest(AnswerRequest $request)
    {
        /**
         * @param AnswerRequest $request
         * @return array;
         */
        $answers = [];
        foreach ($request->answers as $ans) {
            array_push($answers, [
                    'user_id' => auth()->id(),
                    'option_id' => $ans,
                ]
            );
        }
        return $answers;
    }
}
