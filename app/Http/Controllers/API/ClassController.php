<?php


namespace App\Http\Controllers\API;


use App\QClass;
use App\Section;
use App\Set;
use Illuminate\Http\Request;

class ClassController
{
    public function index()
    {
        return QClass::all();
    }

    public function sections($classes)
    {
        return Section::whereIn('class_id', explode(',', $classes))->get();
    }

}
