<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\SetRequest;
use App\Section;
use App\Set;

class SetController
{
    public function index()
    {
        return Set::get();
    }

    public function store(Set $set, SetRequest $request)
    {
        $set = $set->updateOrCreate(['id' => $request->id], ['title' => $request->title]);
        $sections = $request->sections;

        if (!count($sections))
            $sections = Section::whereIn('class_id', $request->classes)->pluck('id')->toArray();

        $set->sections()->sync($sections);

        return $set;
    }

    public function details(Set $set)
    {
        return $set->load('sections');
    }
}
