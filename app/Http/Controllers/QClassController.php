<?php

namespace App\Http\Controllers;

use App\QClass;
use Illuminate\Http\Request;

class QClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QClass  $qClass
     * @return \Illuminate\Http\Response
     */
    public function show(QClass $qClass)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QClass  $qClass
     * @return \Illuminate\Http\Response
     */
    public function edit(QClass $qClass)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QClass  $qClass
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QClass $qClass)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QClass  $qClass
     * @return \Illuminate\Http\Response
     */
    public function destroy(QClass $qClass)
    {
        //
    }
}
